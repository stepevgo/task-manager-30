package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.ITaskRepository;
import ru.t1.stepanishchev.tm.api.service.ITaskService;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @NotNull
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize())
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null)
            throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new TaskIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}